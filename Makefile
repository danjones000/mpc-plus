.PHONY: build

build: builds/mpc-plus-stable

builds/mpc-plus-stable: app/*/*.php config/*.php mpc-plus composer.lock box.json
	env BUILD=true php mpc-plus app:build $(@F) --build-version=stable
	touch $@

composer.lock: composer.json
	composer install
	touch $@
